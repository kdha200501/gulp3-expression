/*
global require
*/
const gulp = require('gulp');
const GulpExpressionParser = require('../../build/Parser/GulpExpressionParser').GulpExpressionParser;

// Create tasks
gulp.task('Aaa', (next) => {
    console.log('Aaa');
    setTimeout(next, 500);
});

gulp.task('Bb', (next) => {
    console.log('Bb');
    next();
});

gulp.task('Ccc', (next) => {
    console.log('Ccc');
    next();
});

gulp.task('D', (next) => {
    console.log('D');
    next();
});

gulp.task('Eeee', (next) => {
    console.log('Eeee');
    next();
});

gulp.task('fFf', (next) => {
    console.log('fFf');
    next();
});

gulp.task('G', (next) => {
    console.log('G');
    next();
});

gulp.task('H', (next) => {
    console.log('H');
    next();
});

let infixExpression = '(Aaa| Bb &Ccc& D)& ( Eeee & fFf |G|H) ';
// let infixExpression = '(Aaa& Bb |Ccc| D)| ( Eeee | fFf &G&H) ';
// let infixExpression = 'Aaa& Bb |Ccc| D|  Eeee | fFf &G&H ';
// let infixExpression = 'Aaa| Bb |Ccc| D|  Eeee | fFf |G|H ';
// let infixExpression = 'Aaa& Bb &Ccc& D&  Eeee & fFf &G&H ';
// let infixExpression = 'Aaa| Bb &(Ccc| D|  Eeee | fFf) &G&H ';
// let infixExpression = 'Aaa & Bb | Ccc & D';
// let infixExpression = 'Aaa';
console.log(
    'original Infix Expression:', infixExpression
);

let gulpExpressionTree = new GulpExpressionParser().parseInfixExpression(infixExpression);
console.log(
    'formatted Infix Expression:', gulpExpressionTree.traverseToBuildGulpTask.format()
);

console.log(
    'Level Order:', gulpExpressionTree.traverseToPrint.exportLevelOrder().map(node => node.key).join(' ')
);
console.log(
    'Prefix Expression:', gulpExpressionTree.traverseToPrint.exportPreOrder().map(node => node.key).join(' ')
);
console.log(
    'Infix Expression:', gulpExpressionTree.traverseToPrint.exportInOrder().map(node => node.key).join(' ')
);
console.log(
    'Postfix Expression:', gulpExpressionTree.traverseToPrint.exportPostOrder().map(node => node.key).join(' ')
);

// Combine tasks
let taskName = gulpExpressionTree.traverseToBuildGulpTask.build();

// console.log(
//     gulp.tasks
// );

// Execute tasks
console.log('execute tasks:');
gulp.start(taskName);
