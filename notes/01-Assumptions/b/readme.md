- can observe task events

```javascript

gulp.on('task_stop', (gulpEvent) => {
    console.log(
        'gulpEvent:', gulpEvent
    );
});

// output:
1
2
gulpEvent: { task: 'Gulp Expression - 2',
  message: 'Gulp Expression - 2 callback',
  duration: 0.000369733,
  hrDuration: [ 0, 369733 ] }
gulpEvent: { task: 'Gulp Expression - 1',
  message: 'Gulp Expression - 1 callback',
  duration: 1.003880953,
  hrDuration: [ 1, 3880953 ] }
3
gulpEvent: { task: 'Gulp Expression - 3',
  message: 'Gulp Expression - 3 callback',
  duration: 0.000119252,
  hrDuration: [ 0, 119252 ] }
gulpEvent: { task: 'default',
  message: 'default sync',
  duration: 0.000029524,
  hrDuration: [ 0, 29524 ] }
```
