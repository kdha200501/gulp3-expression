/*
global require
*/

const gulp = require('gulp');

// register tasks
gulp.task('1', (next) => {
    console.log('1');
    setTimeout(next, 1000);
});

gulp.task('2', (next) => {
    console.log('2');
    next();
});

gulp.task('3', ['1', '2'], (next) => {
    console.log('3');
    next();
});

// re-wrap
gulp.task('Gulp Expression - 1', gulp.tasks['1'].fn);
gulp.task('Gulp Expression - 2', gulp.tasks['2'].fn);
gulp.task('Gulp Expression - 3', ['Gulp Expression - 1', 'Gulp Expression - 2'], gulp.tasks['3'].fn);

gulp.task('default', ['Gulp Expression - 3'], () => {});

gulp.on('task_stop', (gulpEvent) => {
    console.log(
        'gulpEvent:', gulpEvent
    );
});

gulp.start('default');
