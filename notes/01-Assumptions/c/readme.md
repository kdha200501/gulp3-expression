- can add task dependencies to a registered gulp task

```javascript

gulp.tasks['Gulp Expression - 3'].dep.push('Gulp Expression - 1');
gulp.tasks['Gulp Expression - 3'].dep.push('Gulp Expression - 2');
console.log(
    gulp.tasks['Gulp Expression - 3']
);
gulp.start('Gulp Expression - 3');

// output:
{ fn: [Function],
  dep: [ 'Gulp Expression - 1', 'Gulp Expression - 2' ],
  name: 'Gulp Expression - 3' }
1
2
3
```
