- can list registered gulp tasks

```javascript

console.log(
    'gulp.tasks:', gulp.tasks
);

// output:
gulp.tasks: { '1': { fn: [Function], dep: [], name: '1' },
  '2': { fn: [Function], dep: [], name: '2' },
  '3': { fn: [Function], dep: [ '1', '2' ], name: '3' } }
```

- can unwrap a registered gulp task

```javascript

console.log(
    'unwrap:', gulp.tasks['1'].fn
);

// output:
unwrap: (next) => {
    console.log('1');
    setTimeout(next, 1000);
}
```

- can re-wrap tasks

```javascript

gulp.task('Gulp Expression - 1', gulp.tasks['1'].fn);
gulp.task('Gulp Expression - 2', gulp.tasks['2'].fn);
gulp.task('Gulp Expression - 3', ['Gulp Expression - 1', 'Gulp Expression - 2'], gulp.tasks['3'].fn);
console.log(
    'gulp.tasks:', gulp.tasks
);

gulp.task('default', ['Gulp Expression - 3'], () => {});
gulp.start('default');

// output:
1
2
3
```
