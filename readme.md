## Intro.

Gulp provides an interface to create tasks, it also allows developers to combine tasks (in series or in parallel) using its **dependency** feature:

```javascript
// combine tasks in series
gulp.task( 'A', done => {done();} );
gulp.task( 'B', ['A'], done => {done();} );
gulp.task( 'C', ['B'], done => {done();} );

gulp.task('series-demo', ['C']);
```

```javascript
// combine tasks in parallel
gulp.task( 'A', done => {done();} );
gulp.task( 'B', done => {done();} );
gulp.task( 'C', done => {done();} );

gulp.task('parallel-demo', ['A', 'B', 'C']);
```

This npm package provides developers an interface to combine gulp tasks in a **declarative** way as an alternative, it also calculates the overall task completion when an individual task finishes.

```javascript
gulp.task( 'A', done => {done();} );
gulp.task( 'B', done => {done();} );
gulp.task( 'C', done => {done();} );

gulp.task('series-demo', [parseGulpExpression('A | B | C')]);
gulp.task('parallel-demo', [parseGulpExpression('A & B & C')]);
```

> task combination written in gulp3-expression is directly reusable in gulp4-expression, check out [gulp4-expression](https://www.npmjs.com/package/gulp4-expression)

## Installation:
```shellscript
$ npm install gulp3-expression
```

## Usage:
- series: `|`
- parallel: `&`
- sub-expression parentheses: `(...)`

> `&` has a higher precedence than `|`

```javascript
// gulpfile.js

const gulp = require('gulp');
const parseGulpExpression = require('gulp3-expression').parseGulpExpression;

// Register tasks
gulp.task('cleanDir-Build', (done) => {
    console.log('clean directory - /build');
    done();
});

gulp.task('cleanDir-Doc', (done) => {
    console.log('clean directory - /doc');
    done();
});

gulp.task('cleanDir-Coverage', (done) => {
    console.log('clean directory - /coverage');
    done();
});

gulp.task('compileSrc', (done) => {
    console.log('compile directory - /src');
    setTimeout(done, 1000);
});

gulp.task('docSrc', (done) => {
    console.log('document directory - /src');
    setTimeout(done, 1000);
});

gulp.task('test', (done) => {
    console.log('run unit tests - /test');
    setTimeout(done, 1000);
});

// Combine tasks
gulp.task('default', [parseGulpExpression(
    '(cleanDir-Build & cleanDir-Coverage | compileSrc | test) & (cleanDir-Doc | docSrc)'
)]);
```

```shellscript
$ gulp --silent

# output
clean directory - /build
[13:03:07] progress: 8%
clean directory - /coverage
[13:03:07] progress: 17%
compile directory - /src
clean directory - /doc
[13:03:07] progress: 42%
document directory - /src
[13:03:08] progress: 58%
run unit tests - /test
[13:03:08] progress: 83%
[13:03:09] progress: 100%
```

```shellscript
$ gulp

# output
[13:04:12] Starting 'cleanDir-Build (GulpExpressionNodeTask-1)'...
clean directory - /build
[13:04:12] progress: 8%
[13:04:12] Finished 'cleanDir-Build (GulpExpressionNodeTask-1)' after 192 μs
[13:04:12] Starting 'cleanDir-Coverage (GulpExpressionNodeTask-2)'...
clean directory - /coverage
[13:04:12] progress: 17%
[13:04:12] Finished 'cleanDir-Coverage (GulpExpressionNodeTask-2)' after 104 μs
[13:04:12] Starting '<parallel> (GulpExpressionNodeTask-3)'...
[13:04:12] Finished '<parallel> (GulpExpressionNodeTask-3)' after 23 μs
[13:04:12] Starting 'compileSrc (GulpExpressionNodeTask-4)'...
compile directory - /src
[13:04:12] Starting 'cleanDir-Doc (GulpExpressionNodeTask-8)'...
clean directory - /doc
[13:04:12] progress: 42%
[13:04:12] Finished 'cleanDir-Doc (GulpExpressionNodeTask-8)' after 80 μs
[13:04:12] Starting 'docSrc (GulpExpressionNodeTask-9)'...
document directory - /src
[13:04:13] progress: 58%
[13:04:13] Finished 'compileSrc (GulpExpressionNodeTask-4)' after 1.01 s
[13:04:13] Starting '<series> (GulpExpressionNodeTask-5)'...
[13:04:13] Finished '<series> (GulpExpressionNodeTask-5)' after 42 μs
[13:04:13] Starting 'test (GulpExpressionNodeTask-6)'...
run unit tests - /test
[13:04:13] progress: 83%
[13:04:13] Finished 'docSrc (GulpExpressionNodeTask-9)' after 1.01 s
[13:04:13] Starting '<series> (GulpExpressionNodeTask-10)'...
[13:04:13] Finished '<series> (GulpExpressionNodeTask-10)' after 2.35 μs
[13:04:14] progress: 100%
[13:04:14] Finished 'test (GulpExpressionNodeTask-6)' after 1 s
[13:04:14] Starting '<series> (GulpExpressionNodeTask-7)'...
[13:04:14] Finished '<series> (GulpExpressionNodeTask-7)' after 3.24 μs
[13:04:14] Starting '<parallel> (GulpExpressionNodeTask-11)'...
[13:04:14] Finished '<parallel> (GulpExpressionNodeTask-11)' after 2.37 μs
[13:04:14] Starting 'default'...
[13:04:14] Finished 'default' after 54 μs
```
