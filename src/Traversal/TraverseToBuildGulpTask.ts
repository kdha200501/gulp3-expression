import {Traversal} from "./Traversal";
import {GulpExpressionNode} from "../Node/GulpExpressionNode";
import {GulpTaskCombinationEnum} from "../Expression/GulpExpression";
import {addGulpEventListener, GulpTask, IUndertakerEvent, UNDERTAKER_EVENT} from "../Manifest";
import {Segment} from "../lib/Segment";
import {GulpTaskCompletion, TaskFraction} from "../lib/GulpTaskCompletion";
import {Stack} from "../lib/Stack";

const assignSegmentToOperator = (parentNode: GulpExpressionNode, childNode: GulpExpressionNode): void => {
    if(parentNode.isOperand) {
        return;
    }

    if(parentNode.key === GulpTaskCombinationEnum.SERIES && childNode.key === GulpTaskCombinationEnum.SERIES) {
        childNode.segment = parentNode.segment;
        childNode.segment.increase();
    }
    // endIf both the parent and the child node are series operators

    else {
        childNode.segment = new Segment(2);
        childNode.segment.isSegmentOf(parentNode.segment);
    }
    // endIf both the parent and the child node are parallel operators;
    // or the parent and the child node are different operators
};

const assignFractionToOperand = (parentNode: GulpExpressionNode,
                                 childNode: GulpExpressionNode,
                                 taskName: string,
                                 taskFractionMap: TaskFraction): void => {
    if(childNode.isOperand) {
        taskFractionMap[taskName] = parentNode.segment.fraction;
    }
};

export class TraverseToBuildGulpTask extends Traversal<string, GulpExpressionNode> {
    constructor() {
        super();
    }

    // Implement Abstract Methods
    protected onTraverseLevelOrderRec(node: GulpExpressionNode, ...argList) {
        assignSegmentToOperator( node, (<GulpExpressionNode>node.left) );
        assignSegmentToOperator( node, (<GulpExpressionNode>node.right) );
    }

    protected onTraversePreOrderRec(node: GulpExpressionNode, ...argList) {
        let [output] = argList;
        if(!node.isOperand) {
            output.push(node);
            return;
        }
        // endIf the node is an operator

        let top = <GulpExpressionNode>output.peek();
        if(!top || !top.isOperand) {
            output.push(node);
            return;
        }
        // endIf the node is the root or node's previous traversal is a parent i.e. the node is a left operand

        let leftOperand = <GulpExpressionNode>output.pop();
        let operator = <GulpExpressionNode>output.pop();
        top = <GulpExpressionNode>output.peek();
        let operation = new GulpExpressionNode(`(${leftOperand.key}${operator.key}${node.key})`);

        if(top && top.isOperand) {
            this.onTraversePreOrderRec(operation, output);
        }
        // endIf the node's parent has a sibling operand

        else {
            output.push(operation);
        }
        // endIf the node's parent is the root note
    }

    protected onTraverseInOrderRec(node: GulpExpressionNode, ...argList) {
    }

    protected onTraversePostOrderRec(node: GulpExpressionNode, ...argList) {
        let [output, taskFractionMap] = argList;

        if(node.isOperand) {
            output.push(node);
            return;
        }
        // endIf the node is an operand

        let rightOperation = <GulpExpressionNode>output.pop();
        let leftOperation = <GulpExpressionNode>output.pop();
        node.combineGulpTasks(leftOperation, rightOperation);
        assignFractionToOperand(node, <GulpExpressionNode>node.left, leftOperation.taskName, taskFractionMap);
        assignFractionToOperand(node, <GulpExpressionNode>node.right, rightOperation.taskName, taskFractionMap);
        output.push(node);
    }

    // Protected Methods
    protected assignSegmentToOperatorsLevelOrder(): void {
        this.tree.root.segment = new Segment(2);
        this.traverseLevelOrder();
    }

    protected formatPreOrder(): string {
        let output = new Stack<GulpExpressionNode>();
        this.traversePreOrder(output);
        return output.pop().key;
    }

    protected buildPostOrder(): GulpTask {
        let output = new Stack<GulpExpressionNode>();
        let taskFractionMap = <TaskFraction>{};
        this.traversePostOrder(output, taskFractionMap);

        let gulpTaskCompletion = new GulpTaskCompletion(taskFractionMap);
        addGulpEventListener(UNDERTAKER_EVENT.STOP, (undertakerEvent: IUndertakerEvent) => {
            gulpTaskCompletion.updateFractionFromEvent(undertakerEvent);
        });

        return (<GulpExpressionNode>output.pop()).taskName;
    }

    // Public Methods
    public build(): string {
        this.assignSegmentToOperatorsLevelOrder();
        return this.buildPostOrder();
    }

    public format(): string {
        return this.formatPreOrder();
    }
}
