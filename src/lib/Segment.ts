export class Segment {
    constructor(private _count: number = 1) {
    }

    // Private Variables
    private _from: Segment;

    // Public Methods
    public isSegmentOf(segment: Segment): void {
        this._from = segment;
    }

    public increase(increment: number = 1): void {
        this._count += increment;
    }

    public decrease(decrement: number = 1): void {
        this._count = Math.max(1, this._count - decrement);
    }

    // Accessors
    public get fraction(): number {
        return this._from ? this._from.fraction * this._count : this._count;
    }

    public get percentage(): number {
        return 100 / this.fraction;
    }
}
