const formatNumber = (a: number): string => `${a}%`;
const addFractions = (op, fraction) => op += (100 / fraction);

export class Completion {
    constructor() {
        this._fractionList = [];
    }

    // Private Variables
    private _fractionList: Array<number>;

    // Public Methods
    public addFraction(...fractionList): Completion {
        this._fractionList.push(...fractionList);
        return this;
    }

    // Accessors
    public get percentage(): string {
        return formatNumber(
            Math.round( this._fractionList.reduce(addFractions, 0) )
        );
    }
}
