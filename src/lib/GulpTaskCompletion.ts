import {Completion} from "./Completion";
import {IUndertakerEvent, chalk} from "../Manifest";

export type TaskFraction = {
    [taskName: string]: number
}

const formatNumber = (a: number): string => `0${a}`.slice(-2);
const formatTime = (date: Date): string => `${
    formatNumber( date.getHours() )
    }:${
    formatNumber( date.getMinutes() )
    }:${
    formatNumber( date.getSeconds() )
    }`;
const logCompletion = (percentage: string) => console.log(`[${
    chalk.gray( formatTime(new Date()) )
    }] progress: ${
    chalk.cyan( percentage )
    }`);

export class GulpTaskCompletion extends Completion {
    constructor(private _taskFractionMap: TaskFraction) {
        super();
    }

    // Private Methods
    private isTask(undertakerEvent: IUndertakerEvent): boolean {
        return !!this._taskFractionMap[undertakerEvent.task] || this._taskFractionMap[undertakerEvent.task] === 0;
    }

    // Public Methods
    public updateFractionFromEvent(undertakerEvent: IUndertakerEvent) {
        if( this.isTask(undertakerEvent) ) {
            this.addFraction(this._taskFractionMap[undertakerEvent.task]);
            logCompletion(this.percentage);
        }
    }
}
