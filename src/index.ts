import {GulpTask} from "./Manifest";
import {GulpExpressionParser} from "./Parser/GulpExpressionParser";

export {Segment} from "./lib/Segment";

export const parseGulpExpression = (gulpExpression: string): GulpTask =>
    new GulpExpressionParser()
        .parseInfixExpression(gulpExpression)
        .traverseToBuildGulpTask
        .build();
