import {SerializableNode} from "./SerializableNode";
import {getRegisteredTask, registerTask, unwrapTask, parallelTaskKey, seriesTaskKey} from "../Manifest";
import {UnwrappedGulpTask} from "../Manifest";
import {Segment} from "../lib/Segment";
import {GulpTaskCombinationEnum, GulpTaskEnum} from "../Expression/GulpExpression";
import {Counter} from "../lib/Counter";

const counter = new Counter();
const nameTask = (nodeKey:string): string => `${nodeKey} (${GulpTaskEnum.TASK_NAME_PREFIX}-${counter.increment()})`;
const batchPushDependencies = (src: Array<string>, dst: Array<string>): void => {Array.prototype.push.apply(dst, src);};
const prependTaskToDependencies = (topTaskName: string, dependencies: Array<string>): void => {
    dependencies.forEach(taskName => {
        getRegisteredTask(taskName).dep.unshift(topTaskName);
    });
};

const unique = (value, index, self): boolean => self.indexOf(value) === index;

const findEndDependencyRec = (taskName: string, rightDependencies: Array<string>, output: Array<string>): void => {
    if(rightDependencies.length === 0) {
        output.push(taskName);
        return;
    }
    // endIf task has no dependencies
    
    rightDependencies.forEach(_taskName => {
        findEndDependencyRec( _taskName, getRegisteredTask(taskName).dep, output );
    });
    // endEach task dependency
};

export class GulpExpressionNode extends SerializableNode<string>{
    constructor(key?: string) {
        super(key);

        switch(key) {
            case GulpTaskCombinationEnum.SERIES:
                key = seriesTaskKey;
                break;
            case GulpTaskCombinationEnum.PARALLEL:
                key = parallelTaskKey;
                break;
        }

        if( !key || !getRegisteredTask(key) ) {
            return;
        }
        // endIf operand or operator is not registered

        this._unwrappedGulpTask = unwrapTask(key);
        this._taskName = nameTask(key);
        registerTask(this._taskName, null, this._unwrappedGulpTask);
    }

    // Private Variables
    private _taskName: string;
    private _segment: Segment;
    private _unwrappedGulpTask: UnwrappedGulpTask;

    // Public Methods
    public combineGulpTasks(leftOperation: GulpExpressionNode, rightOperation: GulpExpressionNode): void {
        switch(this.key) {
            case GulpTaskCombinationEnum.SERIES:

                if( leftOperation.key === GulpTaskCombinationEnum.PARALLEL &&
                    rightOperation.key === GulpTaskCombinationEnum.PARALLEL ) {
                    let operation = new GulpExpressionNode(GulpTaskCombinationEnum.SERIES);
                    batchPushDependencies(leftOperation.taskDependencies, operation.taskDependencies);
                    prependTaskToDependencies(operation.taskName, rightOperation.taskDependencies);
                }
                // endIf both child nodes are parallel operators

                else if(rightOperation.key === GulpTaskCombinationEnum.PARALLEL) {
                    prependTaskToDependencies(leftOperation.taskName, rightOperation.taskDependencies);
                }
                // endIf the right child nodes is a parallel operator

                else if( leftOperation.key === GulpTaskCombinationEnum.SERIES &&
                         rightOperation.key === GulpTaskCombinationEnum.SERIES ) {
                    let rightDependencies = [];
                    findEndDependencyRec(rightOperation.taskName, rightOperation.taskDependencies, rightDependencies);
                    rightDependencies.filter(unique).forEach(taskName => {
                        getRegisteredTask(taskName).dep.push(leftOperation.taskName);
                    });
                }
                // endIf both child nodes are series operators

                else {
                    rightOperation.taskDependencies.unshift(leftOperation.taskName);
                }
                // endIf the left child nodes is a parallel operator, or both child nodes are operands

                this.taskDependencies.push(rightOperation.taskName);
                break;
            case GulpTaskCombinationEnum.PARALLEL:

                if( leftOperation.key === GulpTaskCombinationEnum.PARALLEL &&
                    rightOperation.key === GulpTaskCombinationEnum.PARALLEL ) {
                    batchPushDependencies(leftOperation.taskDependencies, this.taskDependencies);
                    batchPushDependencies(rightOperation.taskDependencies, this.taskDependencies);
                }
                // endIf both child nodes are parallel operators

                else if(leftOperation.key === GulpTaskCombinationEnum.PARALLEL) {
                    batchPushDependencies(leftOperation.taskDependencies, this.taskDependencies);
                    this.taskDependencies.push(rightOperation.taskName);
                }
                // endIf the left child nodes is a parallel operator

                else if(rightOperation.key === GulpTaskCombinationEnum.PARALLEL) {
                    this.taskDependencies.push(leftOperation.taskName);
                    batchPushDependencies(rightOperation.taskDependencies, this.taskDependencies);
                }
                // endIf the right child nodes is a parallel operator

                else {
                    this.taskDependencies.push(leftOperation.taskName);
                    this.taskDependencies.push(rightOperation.taskName);
                }
                // endIf both child nodes are operands

                break;
            default:
                throw new Error('createTask is applicable to operator, only');
        }
    }

    // Accessors
    public get taskName(): string {
        return this._taskName;
    }

    public get segment(): Segment {
        return this._segment;
    }

    public set segment(segment: Segment) {
        this._segment = segment;
    }

    public get taskDependencies(): Array<string> {
        return getRegisteredTask(this._taskName).dep;
    }

    public get isOperand(): boolean {
        return this.isNull ? false : this.left.isNull;
    }
}
