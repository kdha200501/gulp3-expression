export enum GulpTaskCombinationEnum {
    SERIES = '|',
    PARALLEL = '&',
    OPEN_PARENTHESIS = '(',
    CLOSE_PARENTHESIS = ')',
}

export enum GulpTaskEnum {
    TASK_NAME_PREFIX = 'GulpExpressionNodeTask'
}

export const GulpTaskCombinationWeight = {};
// the minimum weight is 1, the higher the weight, the higher the precedence
GulpTaskCombinationWeight[GulpTaskCombinationEnum.SERIES] = 1;
GulpTaskCombinationWeight[GulpTaskCombinationEnum.PARALLEL] = 2;

const optionalSpace = '[\\s]*';
const anyOperator = `(${
    Object.keys(GulpTaskCombinationEnum)
        .map(key => ['\\', ''].join(GulpTaskCombinationEnum[key]))
        .join('|')
    })`;

const operatorBreak = ['', '\\B', ''].join(optionalSpace);

const leadingRepeatingOperator = `${anyOperator}${operatorBreak}`;
const trailingRepeatingOperator = `${operatorBreak}${anyOperator}`;

const nonCharBoundary = '([^\\w\~!@#$%^*+`\\-={}\\[\\]";\\\'<>?,./:])';// \\W or none of ~!@#$%^*_+`-={}[]\:";\'<>?,./
export const tokenRegExp = new RegExp(`${nonCharBoundary}|${leadingRepeatingOperator}|${trailingRepeatingOperator}`);
