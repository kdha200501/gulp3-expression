import {task} from "gulp";
export type GulpTask = task|string;
export type UnwrappedGulpTask = (...argList)=>{};

import * as gulpInstance from "gulp";
export const addGulpEventListener = (...argList) => gulpInstance.on(...argList);
export const getRegisteredTask = (taskName: string) => gulpInstance.tasks[taskName];
export const registerTask = (taskName: string, dep: Array<string> = null, unwrappedTask: (...argList)=>{}) => dep === null ?
    gulpInstance.task(taskName, unwrappedTask) :
    gulpInstance.task(taskName, dep, unwrappedTask);
export const unwrapTask = (taskName: string) => getRegisteredTask(taskName).fn;

export const seriesTaskKey = '<series>';
gulpInstance.task(seriesTaskKey, next => {
    next();
});
export const parallelTaskKey = '<parallel>';
gulpInstance.task(parallelTaskKey, next => {
    next();
});

export interface IUndertakerEvent {
    task: string
}

export enum UNDERTAKER_EVENT {
    START = 'task_start',
    STOP = 'task_stop',
    ERROR = 'task_err'
}

import * as chalkInstance from "chalk";
export const chalk = chalkInstance;
