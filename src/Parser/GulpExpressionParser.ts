import {Parser} from "./Parser";
import {GulpTaskCombinationEnum, GulpTaskCombinationWeight, tokenRegExp} from "../Expression/GulpExpression";
import {GulpExpressionNode} from "../Node/GulpExpressionNode";
import {GulpExpressionTree} from "../Tree/GulpExpressionTree";
import {Stack} from "../lib/Stack";

const filterNonEmptyToken = (op: Array<string>, token: string) => {
    token = token ? token.trim() : null;
    if(token) {
        op.push(token);
    }
    return op;
};

export class GulpExpressionParser extends Parser<string, GulpExpressionNode, GulpExpressionTree> {
    constructor() {
        super(GulpExpressionTree);
    }

    // Implement Protected Abstract Variables
    protected get openParenthesis(): string {
        return GulpTaskCombinationEnum.OPEN_PARENTHESIS;
    }

    protected get closeParenthesis(): string {
        return GulpTaskCombinationEnum.CLOSE_PARENTHESIS;
    }

    // Implement Abstract Methods
    protected buildOperatorNode(operator: string, nodeStack: Stack<GulpExpressionNode>): void {
        // the top node of an expression sub-tree must be an operator
        let node = new GulpExpressionNode(operator);
        node.right = nodeStack.pop();
        node.left = nodeStack.pop();
        nodeStack.push(node);
    }

    protected buildOperandNode(token: string, nodeStack: Stack<GulpExpressionNode>): void {
        let operand = token.trim();
        nodeStack.push( new GulpExpressionNode(operand) );
    }

    protected splitToken(expressionStr: string): Array<string> {
        return expressionStr.split(tokenRegExp).reduce(filterNonEmptyToken, []);
    }

    protected tokenIsAnOperator(token: string): boolean {
        return !!GulpTaskCombinationWeight[token];
    }

    protected tokenIsLowerPrecedence(a: string, b: string): boolean {
        return GulpTaskCombinationWeight[a] <= GulpTaskCombinationWeight[b];
    }
}
