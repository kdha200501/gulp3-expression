import {Tree} from "./Tree";
import {GulpExpressionNode} from "../Node/GulpExpressionNode";
import {TraverseToBuildGulpTask} from "../Traversal/TraverseToBuildGulpTask";

export class GulpExpressionTree extends Tree<string, GulpExpressionNode> {

    public constructor() {
        super(GulpExpressionNode);

        this._traverseToBuildGulpTask = new TraverseToBuildGulpTask();
        this._traverseToBuildGulpTask.tree = this;
    }

    // Private Variables
    private _traverseToBuildGulpTask: TraverseToBuildGulpTask;

    // Accessors
    public get traverseToBuildGulpTask(): TraverseToBuildGulpTask {
        return this._traverseToBuildGulpTask;
    }
}
